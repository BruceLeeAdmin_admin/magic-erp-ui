import request from '@/utils/request'

export const props = {
  // 显示选择器
  show: {
    type: Boolean,
    default: false
  },
  // 已选导购的ID集合
  selectedIds: {
    type: Array,
    required: false,
    default: () => ([])
  },
  // 获取导购列表API
  goodsApi: {
    type: String,
    required: true
  },
  // 扩展的导购API参数
  purchasePlanApiParams: {
    type: Object,
    default: () => ({})
  },
  // 请求方法
  request: {
    type: Function,
    required: false,
    default: request
  },
  // 最大可选个数
  limit: {
    type: Number,
    default: -1
  },
  // 扩展列
  columns: {
    type: Array,
    default: () => (generalColumns)
  },
  // 是否是管理端
  isAdmin: {
    type: Boolean,
    default: true
  },
	// 调出仓选择数据
	warehouseList: {
		type: Array,
		default: []
	},
	// 是否显示仓库搜索
	showWarehouseSearch: {
		type: Boolean,
		default: true
	}

}

export const data = {
}

// 一般列
export const generalColumns = [
  { label: '入库单号', prop: 'warehouse_entry_sn' },
  { label: '商品编号', prop: 'product_sn' },
  { label: '商品名称', prop: 'product_name' },
  { label: '规格型号', prop: 'product_specification' },
  { label: '入库数量', prop: 'entry_num' },
  { label: '库存数量', prop: 'remain_num' }
]
