/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import warehouseEntry from './src/main.vue'
import PickerWarehouseEntry from './src/main.js'

warehouseEntry.install = function() {
  Vue.component(warehouseEntry.name, warehouseEntry)
}
Vue.prototype.$EnwarehouseEntry = PickerWarehouseEntry

export default warehouseEntry
