/**
 * 导出选择器
 */
import TableLayout from './TableLayout'
import TableSearch from './TableSearch'
import RegionPicker from './RegionPicker'
import PurchasePlan from './PurchasePlan'
import PickerGoods from './PickerGoods'
import PickerPlan from './PickerPlan'
import PickerContract from './PickerContract'
import PickerWarehouseEntryBatch from './PickerWarehouseEntryBatch'
import PickerOrder from './PickerOrder'
import PickerWarehouseEntry from './PickerWarehouseEntry'
import PickerWarehouseOut from './PickerWarehouseOut'
import PickerSupplier from './PickerSupplier'
import GradeEditor from './GradeEditor'
import TransferTree from './TransferTree'
import GoodsPicker from './GoodsPicker'

let components = {
  TableLayout,
  TableSearch,
  RegionPicker,
  PurchasePlan,
  PickerGoods,
  PickerPlan,
  PickerWarehouseEntryBatch,
  PickerWarehouseEntry,
  PickerWarehouseOut,
  PickerOrder,
  PickerSupplier,
  GradeEditor,
  TransferTree,
  GoodsPicker
}

components.install = function(Vue, opts) {
  Object.keys(components).forEach(function(key) {
    key !== 'install' && Vue.component(components[key].name, components[key])
  })
}

export {
  TableLayout,
  TableSearch,
  RegionPicker,
  PurchasePlan,
  PickerGoods,
  PickerPlan,
  PickerContract,
  PickerWarehouseEntryBatch,
  PickerWarehouseEntry,
  PickerWarehouseOut,
  PickerOrder,
  PickerSupplier,
  GradeEditor,
  TransferTree,
  GoodsPicker
}

export default components
