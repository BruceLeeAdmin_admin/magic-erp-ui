/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PickerGoods from './src/main.vue'
import PickerGoodsPicker from './src/main.js'

PickerGoods.install = function() {
  Vue.component(PickerGoods.name, PickerGoods)
}
Vue.prototype.$EnPickerGoods = PickerGoodsPicker

export default PickerGoods
