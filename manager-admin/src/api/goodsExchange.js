/**
 * 商品换货相关API
 */

import request from '@/utils/request'

/**
 * 归还商品换货
 * @param id
 * @param parmas
 */
export function returnGoodsExchange(id, parmas) {
  return request({
    url: `admin/change/form/return/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 批量确认商品换货单
 * @param id
 */
export function changeFormConfirm(ids) {
  return request({
    url: `admin/change/form/confirm/${ids}`,
    method: 'post'
  })
}

/**
 * 修改商品换货
 * @param id
 * @param parmas
 */
export function editgoodsExchangeAdd(id, parmas) {
  return request({
    url: `admin/change/form/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加商品换货
 * @param params
 */
export function addgoodsExchangeAdd(params) {
  return request({
    url: 'admin/change/form',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 提交审核商品换货单
 * @param ids
 */
export function submitAuditGoodsExchange(ids) {
  return request({
    url: `admin/change/form/submit/${ids}`,
    method: 'post'
  })
}

/**
 * 撤回商品换货
 * @param ids
 */
export function cancelGoodsExchange(ids) {
  return request({
    url: `admin/change/form/cancel/${ids}`,
    method: 'post'
  })
}

/**
 * 审核商品换货
 * @param params
 */
export function auditGoodsExchange(ids, parmas) {
  return request({
    url: `admin/change/form/audit/${ids}`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取商品换货详情
 * @param params
 */
export function getGoodsExchangeInfo(id) {
  return request({
    url: `admin/change/form/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取商品换货列表
 * @param params
 */
export function getGoodsExchangeList(params) {
  return request({
    url: 'admin/change/form',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 更新商品换货
 * @param id
 * @param parmas
 */
export function editGoodsExchange(id, parmas) {
  return request({
    url: `admin/change/form/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除商品换货
 * @param id
 */
export function deleteGoodsExchange(ids) {
  return request({
    url: `admin/change/form/${ids}`,
    method: 'delete'
  })
}
