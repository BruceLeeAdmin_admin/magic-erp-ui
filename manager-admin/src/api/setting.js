/**
 * 设置相关API
 */

import request from '@/utils/request'

/**
 * 查询系统日志列表
 * @param params
 */
export function getSystemLogs(params) {
  return request({
    url: 'admin/system-logs',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 获取岗位列表
 * @param params
 */
export function getStationList(params) {
  return request({
    url: 'admin/erp/post',
    method: 'get',
    params
  })
}

/**
 * 获取岗位列表全部
 * @param params
 */
export function getStationAllList() {
  return request({
    url: 'admin/erp/post/list-all',
    method: 'get',
    loaidng: false
  })
}

/**
 * 添加岗位
 * @param params
 */
export function addStation(parmas) {
  return request({
    url: `admin/erp/post`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新岗位
 * @param id
 * @param parmas
 */
export function editStation(id, parmas) {
  return request({
    url: `admin/erp/post/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除岗位
 * @param id
 */
export function deleteStation(id) {
  return request({
    url: `admin/erp/post/${id}`,
    method: 'delete'
  })
}

/**
 * 批量删除岗位
 * @param id
 */
export function deletesStation(ids) {
  return request({
    url: `admin/erp/post/${ids}`,
    method: 'delete'
  })
}

/**
 * 获取部门列表
 * @param params
 */
export function getDeptList() {
  return request({
    url: 'admin/systems/dept/list',
    method: 'get'
  })
}

/**
 * 添加部门
 * @param params
 */
export function addDept(parmas) {
  return request({
    url: `admin/systems/dept/create?region=${parmas.region}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新部门
 * @param id
 * @param parmas
 */
export function editDept(parmas) {
  return request({
    url: `admin/systems/dept/update?region=${parmas.region}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除部门
 * @param id
 */
export function deleteDept(params) {
  return request({
    url: `admin/systems/dept/delete`,
    method: 'delete',
    params
  })
}

/**
 * 获取字典类型列表
 * @param params
 */
export function getDictTypeList(params) {
  return request({
    url: 'admin/systems/dict-type/page',
    method: 'get',
    params
  })
}

/**
 * 添加字典类型
 * @param params
 */
export function addDictType(params) {
  return request({
    url: 'admin/systems/dict-type/create',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 修改字典类型
 * @param id
 * @param parmas
 */
export function editDictType(parmas) {
  return request({
    url: `admin/systems/dict-type/update`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除字典类型
 * @param id
 */
export function deleteDictType(params) {
  return request({
    url: `admin/systems/dict-type/delete`,
    method: 'delete',
    params
  })
}

/**
 * 获取指定字段类型字典数据列表
 * @param params
 */
export function getDictDataInfo(dict_type) {
  return request({
    url: 'admin/systems/dict-data/list-all-simple',
    method: 'get',
    loaidng: false,
    params: {
      dict_type
    }
  })
}

/**
 * 获取字典数据列表
 * @param params
 */
export function getDictDataList(params) {
  return request({
    url: 'admin/systems/dict-data/page',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 修改字典数据
 * @param id
 * @param parmas
 */
export function editDictData(parmas) {
  return request({
    url: `admin/systems/dict-data/update`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加字典数据
 * @param params
 */
export function addDictData(params) {
  return request({
    url: 'admin/systems/dict-data/create',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除字典数据
 * @param id
 */
export function deleteDictData(params) {
  return request({
    url: `admin/systems/dict-data/delete`,
    method: 'delete',
    params
  })
}
